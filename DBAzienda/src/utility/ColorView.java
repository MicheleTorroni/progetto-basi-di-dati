package utility;

import java.awt.Color;

public class ColorView {

	public static Color myOrange() {
		return new Color(234, 89, 36);
	}

	public static Color myGray() {
		return new Color(74, 74, 74);
	}

}
